# Ubisoft-DNA Fullstack technical test

For this project, you will be fetching and displaying data from the Twitch API.
You can find the documentation for the Twitch API [here](https://dev.twitch.tv/docs/api/)

**Please note that in order to access the twitch API you'll have to create a Twitch account.**

## Objective

Your goal will be to build a simple web dashboard with the following 2 features:

1. You will need to create a Web Socket connection to display the number of viewers for Rainbow Six Siege that automatically updates a counter in real time (keep in mind the request throttling limit of the Twitch API).
2. We would like a line Chart that compares in real time the number of viewers for the following 3 games:
    - Rainbow Six Siege
https://www.twitch.tv/directory/game/Tom%20Clancy's%20Rainbow%20Six%3A%20Siege
    - Far Cry 5
https://www.twitch.tv/directory/game/Far%20Cry%205
    - Assassin’s Creed Odyssey
https://www.twitch.tv/directory/game/Assassin's%20Creed%20Odyssey

## Technology

Frontend: **Angular/RxJS/Typescript** 
Backend: **your choice**

## Rules 

The project should not take more than 1 week.
To submit your project, please push the code in this repository (You'll have access to this repository for a week). 
Also, please have the project hosted online using a service such as Heroku and provide us with the link. 
You are encouraged to submit all of your work at the end of the 1 week period even if it is not complete.
 
- document your API design (routes + payload)
- document your code only if needed
- document the steps required to run your project or to run tests
- use Markdown to share your documentation inside this code repository

Good luck, be creative and have fun :)
