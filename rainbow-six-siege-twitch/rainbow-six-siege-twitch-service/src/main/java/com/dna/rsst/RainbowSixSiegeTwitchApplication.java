package com.dna.rsst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowSixSiegeTwitchApplication {
    public static void main(String[] args) {
        SpringApplication.run(RainbowSixSiegeTwitchApplication.class, args);
    }
}
