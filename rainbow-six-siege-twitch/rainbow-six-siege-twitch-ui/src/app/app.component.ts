import { Component } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Observable } from 'rxjs';

interface Message {
  name: string; message: string; type: string;
}

@Component({
  selector: 'app-root',
  template: '<app-curve-chart></app-curve-chart>'
})
export class AppComponent implements OnInit{
  messages: Message[] = [];
  name: string;
  message: string;
}
