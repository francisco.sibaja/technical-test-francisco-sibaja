import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LineChartComponent } from './line-chart.component';

@NgModule({
  declarations: [
    AppComponent, LineChartComponent
  ],
  imports: [
    BrowserModule
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
