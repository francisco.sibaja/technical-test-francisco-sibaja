import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
declare var google: any;

@Component({
  selector: 'app-curve-chart',
  template: '<div #curveChart style="width: 100vw; height: 100vh;"></div>'
})
export class LineChartComponent implements AfterViewInit{

  @ViewChild('curveChart') curveChart: ElementRef

  drawChart = () => {

  const data = google.visualization.arrayToDataTable([
                         ['Year', 'Sales', 'Expenses'],
                         ['2004',  1000,      400],
                         ['2005',  1170,      460],
                         ['2006',  660,       1120],
                         ['2007',  1030,      540]
                       ]);

  const options = {
    title : "Rainbow Six Siege Twitch",
    curveType: 'function',
    legend: {position: 'top'}
  };

  const chart = new google.visualization.LineChart(this.curveChart.nativeElement);

  chart.draw(data, options);
}

  ngAfterViewInit() {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(this.drawChart);
  }
}
