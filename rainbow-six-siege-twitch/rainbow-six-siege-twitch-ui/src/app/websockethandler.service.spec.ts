import { TestBed } from '@angular/core/testing';

import { WebsockethandlerService } from './websockethandler.service';

describe('WebsockethandlerService', () => {
  let service: WebsockethandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsockethandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
