import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";
import { WebsocketService } from "./websocket.service";

const SERVICE_URL = "ws://localhost:8080/rsst";

export interface Message {
  author: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class WebsockethandlerService {
  public messages: Subject<Message>;

  constructor(wsService: WebsocketService) {
    this.messages = <Subject<Message>>wsService.connect(SERVICE_URL).map(
      (response: MessageEvent): Message => {
        return response.data
      }
    );
  }
}
